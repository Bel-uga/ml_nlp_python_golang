package workerPool

import (
	"context"
	"sync"
)

// Worker представляет отдельного воркера в пуле
type Worker struct {
	id          int
	workerQueue chan Request
}

type Request struct {
	Data       map[string]interface{}
	ResultChan chan map[string]interface{}
}

// WorkerPool представляет пул воркеров
type WorkerPool struct {
	workers     []*Worker
	workerQueue chan Request
}

// NewWorker создает нового воркера с заданным ID и каналом для получения задач
func NewWorker(id int, workerQueue chan Request) *Worker {
	return &Worker{
		id:          id,
		workerQueue: workerQueue,
	}
}

// Start запускает воркера для обработки задач
func (w *Worker) Start(ctx context.Context, wg *sync.WaitGroup, processRequest func(ctx context.Context, requestData Request, workerID int)) {
	defer wg.Done()
	for {
		select {
		case requestData := <-w.workerQueue:
			processRequest(ctx, requestData, w.id)
		case <-ctx.Done():
			return
		}
	}
}

// NewWorkerPool создает новый пул воркеров с заданным количеством воркеров, размером очереди и функцию для обработки задач
func NewWorkerPool(numWorkers, maxQueueSize int, processRequest func(ctx context.Context, requestData Request, workerID int)) *WorkerPool {
	workerQueue := make(chan Request, maxQueueSize)

	pool := &WorkerPool{
		workers:     make([]*Worker, numWorkers),
		workerQueue: workerQueue,
	}

	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}

	// Создаем воркеров
	for i := 0; i < numWorkers; i++ {
		pool.workers[i] = NewWorker(i+1, workerQueue)
		wg.Add(1)
		go pool.workers[i].Start(ctx, &wg, processRequest)
	}

	// Ожидаем завершения работы всех воркеров
	go func() {
		wg.Wait()
		cancel()
	}()

	return pool
}

// Process добавляет задачу в очередь пула воркеров
func (p *WorkerPool) Process(request Request) {
	p.workerQueue <- request
}
