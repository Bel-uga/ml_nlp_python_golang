package main

import (
	"log"
	"main/controllers"
	"main/router"
	"main/workerPool"
	"os"

	"github.com/joho/godotenv"
	"github.com/valyala/fasthttp"
)

const (
	numWorkers   = 5  // кол-во воркеров
	maxQueueSize = 10 // длина очереди (ожидаемая максимальная нагрузка)
)

var Pool = workerPool.NewWorkerPool(numWorkers, maxQueueSize, controllers.ProcessRequest)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	err := fasthttp.ListenAndServe(
		":"+port,
		func(ctx *fasthttp.RequestCtx) {
			setHeaderCORS(ctx)
			router := router.Router(Pool)
			router.Handler(ctx)
		},
	)

	if err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

func setHeaderCORS(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Authorization, Access-Control-Allow-Origin, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	ctx.Response.Header.Set("Access-Control-Allow-Credentials", "true")
	ctx.Response.Header.Set("Access-Control-Allow-Origin", os.Getenv("frontend_url"))
	ctx.Response.Header.Add("Content-Type", "application/json")
}
