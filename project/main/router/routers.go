package router

import (
	"main/controllers"
	"main/workerPool"

	"github.com/fasthttp/router"
)

func Router(pool *workerPool.WorkerPool) *router.Router {
	router := router.New()
	router.HandleOPTIONS = true

	router.GET("/process", controllers.RunProcess(pool))

	return router
}
