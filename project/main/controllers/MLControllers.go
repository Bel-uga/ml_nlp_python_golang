package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"main/workerPool"
	"os"

	"github.com/valyala/fasthttp"
)

type Result struct {
	Label string  `json:"label"`
	Score float64 `json:"score"`
}

type Request struct {
	Text string `json:"text"`
}

func ProcessRequest(ctx context.Context, requestData workerPool.Request, workerID int) {
	url := fmt.Sprintf("%v", requestData.Data["url"])
	request := &Request{
		Text: fmt.Sprintf("%v", requestData.Data["text"]),
	}

	body, _ := json.Marshal(request)
	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.SetRequestURI(url)
	req.Header.Set("Content-Type", "application/json")
	req.SetBodyRaw(body)
	resp := fasthttp.AcquireResponse()
	if err := fasthttp.Do(req, resp); err != nil {
		log.Fatalf("Error in fasthttp.Do: %s", err)
	}
	var result map[string]interface{}
	err := json.Unmarshal([]byte(resp.Body()), &result)
	if err != nil {
		log.Fatalf("Error decoding JSON: %s", err)
	}
	requestData.ResultChan <- map[string]interface{}{
		fmt.Sprintf("%v", requestData.Data["name"]): result,
	}
	fasthttp.ReleaseRequest(req)
	fasthttp.ReleaseResponse(resp)
}

func RunProcess(pool *workerPool.WorkerPool) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		response := []map[string]interface{}{}
		result := make(chan map[string]interface{})
		text := string(ctx.FormValue("text"))
		var models = []map[string]interface{}{
			{
				"url":  os.Getenv("model_1"),
				"name": "jy46604790",
			},
			{
				"url":  os.Getenv("model_2"),
				"name": "samLowe",
			},
		}

		request := workerPool.Request{
			ResultChan: result,
		}

		for _, value := range models {
			request.Data = value
			request.Data["text"] = text
			pool.Process(request)
		}

		for i := 0; i < len(models); i++ {
			response = append(response, <-result)
		}

		close(result)
		json.NewEncoder(ctx).Encode(response)
	}
}
