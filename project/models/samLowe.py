from transformers import pipeline


async def run(text: str):
    model_path = "SamLowe/roberta-base-go_emotions"
    sentiment_task = pipeline(
        "sentiment-analysis",
        model=model_path,
        tokenizer=model_path)

    return sentiment_task(text)[0]
