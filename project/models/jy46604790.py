from transformers import pipeline


async def run(text: str):
    model_path = "jy46604790/Fake-News-Bert-Detect"
    sentiment_task = pipeline(
        "sentiment-analysis",
        model=model_path,
        tokenizer=model_path)

    return sentiment_task(text)[0]
