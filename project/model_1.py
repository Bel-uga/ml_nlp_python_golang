from fastapi import Body, FastAPI
import uvicorn
import models

app = FastAPI()


@app.post("/process")
async def run(data=Body()):
    text = data["text"]
    return await models.jy46604790.run(text)

if __name__ == "__main__":
    uvicorn.run("model_1:app", host="0.0.0.0", port=8001, reload=True)
