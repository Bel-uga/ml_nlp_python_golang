from fastapi import Body, FastAPI
import uvicorn
import models

app = FastAPI()


@app.post("/process")
async def run(data=Body()):
    print("process")
    text = data["text"]
    return await models.samLowe.run(text)

if __name__ == "__main__":
    uvicorn.run("model_2:app", host="0.0.0.0", port=8002, reload=True)
